import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import VueMeta from 'vue-meta'
import 'vue-swatches/dist/vue-swatches.css'

Vue.config.productionTip = false

Vue.use(VueMaterial)

Vue.use(VueMeta)


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
